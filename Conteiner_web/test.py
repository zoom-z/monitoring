#тесты для проверки установки нужного ПО и файлов


import pytest
import testinfra

def test_OS (host):
    assert host.file("/etc/os-release").contains("Alpine")

def test_nginx (host):
    nginx=host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")

# def test_nginx_running_and_enabled(host):
#     assert host.service('nginx').is_running
  
def test_node_exporter (host):
    assert host.file("/usr/local/bin/node_exporter/node_exporter").exists


#def test_promtail (host):
 #   assert host.file("/usr/local/bin/promtail/promtail").exists

#def test_promtail_service_is_running(host):
 #   assert host.service('promtail').is_running 

